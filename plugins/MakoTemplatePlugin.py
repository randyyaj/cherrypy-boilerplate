import cherrypy
import shutil

from cherrypy.process import wspbus, plugins
from mako.template import Template
from mako.lookup import TemplateLookup
from pathlib import Path

__author__ = "Randy Yang (https://www.gitlab.com/randyyaj)"
__license__ = "MIT"
__version__ = "0.1.0"


class MakoTemplatePlugin(plugins.SimplePlugin):
    """
    Custom Mako Template Compiler Plugin for cherrypy.

    Register this plugin after the cherrypy.engine.start() block in your 
    webservice or place it in your main function in your app.
    """

    def __init__(self, bus, base_dir=None, base_cache_dir='/tmp/mako_modules', collection_size=50, encoding='utf-8'):
        plugins.SimplePlugin.__init__(self, bus)
        
        if (Path(base_cache_dir).exists == True):
            shutil.rmtree(base_cache_dir)

        self.base_dir = base_dir
        self.base_cache_dir = base_cache_dir
        self.collection_size = collection_size
        self.encoding = encoding
        self.lookup_template = None

    def start(self):
        self.bus.log('Starting MakoTemplatePlugin')
        self.template_lookup = TemplateLookup(
            directories=self.base_dir,
            module_directory=self.base_cache_dir,
            output_encoding=self.encoding,
            encoding_errors='replace',
            collection_size=self.collection_size)

        self.bus.subscribe("mako-lookup-template", self.get_template)

    def stop(self):
        self.bus.log('Stopping MakoTemplatePlugin')
        self.bus.unsubscribe("mako-lookup-template", self.get_template)
        self.lookup_template = None

    def get_template(self, template_name):
        template = self.template_lookup.get_template(template_name)
        return template