import cherrypy
import shutil

from cherrypy.process import wspbus, plugins
from jinja2 import Environment, FileSystemLoader, select_autoescape
from pathlib import Path

__author__ = "Randy Yang (https://www.gitlab.com/randyyaj)"
__license__ = "MIT"
__version__ = "0.1.0"

class JinjaTemplatePlugin(plugins.SimplePlugin):
    """
    Custom Jinja Template Compiler Plugin for cherrypy.

    Register this plugin after the cherrypy.engine.start() block in your 
    webservice or place it in your main function in your app.
    """

    def __init__(self, bus, template_dirs=None, cache_size=0):
        plugins.SimplePlugin.__init__(self, bus)
        self.template_dirs = template_dirs
        self.cache_size = cache_size

        self.env = Environment(loader=FileSystemLoader(self.template_dirs, encoding='utf-8'), 
            autoescape=select_autoescape(['html', 'xml']),
            cache_size=self.cache_size,
            trim_blocks=True)

    def start(self):
        self.bus.log('Starting JinjaTemplatePlugin')
        self.bus.subscribe("jinja-lookup-template", self.get_template)

    def stop(self):
        self.bus.log('Stopping JinjaTemplatePlugin')
        self.bus.unsubscribe("jinja-lookup-template", self.get_template)
        self.env = None

    def get_template(self, template_name):
        template = self.env.get_template(template_name)
        return template