# CherryPy Boilerplate
> CherryPy Boilerplate. Includes heroku deploy, multi-app setup, sass plugin, and mako and jinja templating

## Technology Stack Prerequisites
 - [python 3.6+](https://www.python.org/downloads/)
 - [heroku](https://www.heroku.com/)
 - [git](https://www.git-scm.com)

## What does this project include
 - Multiple Application handling
 - Sass Compiler Plugin
 - Jinja2 and Mako template plugin/tool

## How to run app
To run the app make sure python 3.6 or higher is installed and [pipenv](https://docs.pipenv.org/basics/) is installed. Set the python path in your .bashrc or .bash_profile

```
python3path=`python3 -m site --user-base`
PATH=$PATH:~/bin:$python3path
```

Source your bash changes:

`$ source .bashrc`

Install python dependencies from pipfile:

`$ pipenv install`

Run the app:

`$ pipenv run python3 server.py`

> Using $ pipenv run ensures that your installed packages are available to your script. It’s also possible to spawn a new shell that ensures all commands have access to your installed packages with $ pipenv shell.

## Pushing to Heroku
To run the app locally: 

`$ heroku local web`

[Pushing python apps to Heroku](https://devcenter.heroku.com/articles/getting-started-with-python)