from plugins.SassCompilerPlugin import SassCompilerPlugin
from plugins.MakoTemplatePlugin import MakoTemplatePlugin
from tools.MakoTemplateTool import MakoTemplateTool
from plugins.JinjaTemplatePlugin import JinjaTemplatePlugin
from tools.JinjaTemplateTool import JinjaTemplateTool

import cherrypy
import os


class WebService(object):

    def __init__(self):
        SassCompilerPlugin(cherrypy.engine).subscribe() #Register Sass compiler plugin
        MakoTemplatePlugin(cherrypy.engine, ['webapps/app1/view']).subscribe()  #Register Mako Plugin
        JinjaTemplatePlugin(cherrypy.engine, ['webapps/app2/view']).subscribe() #Register Jinja2 Plugin

    def __mount_applications(self):
        """
        Mount applications here. The import happens here because these applications
        need all tools and plugins to be registered first.
        """
        from webapps.app1.app import Application as Application1
        from webapps.app2.app import Application as Application2

        cherrypy.tree.mount(Application1(), '/app1', 'webapps/app1/app.cfg')
        cherrypy.tree.mount(Application2(), '/app2', 'webapps/app2/app.cfg')

    # a blocking call that starts the web application listening for requests
    def start(self, port=8090):
        cherrypy.config.update(os.path.join(os.getcwd(), "server.cfg")) #Source the server.cfg configuration properties
        cherrypy.config.update({'server.socket_port': port})
        cherrypy.engine.start()

        cherrypy.engine.publish('compile_sass')  #Register sass compiler tool
        cherrypy.tools.mako = MakoTemplateTool() #Register mako template tool
        cherrypy.tools.jinja = JinjaTemplateTool() #Register jinja template tool

        self.__mount_applications() #Call mount_applications to register applications

        cherrypy.engine.block()

    # stops the web application
    def stop(self):
        cherrypy.engine.stop()

webservice = WebService()
webservice.start()
