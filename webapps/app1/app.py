import random
import string
import cherrypy

class Application(object):
    def __init__(self):
        self.api = Api()

    @cherrypy.expose
    @cherrypy.tools.mako(template_name='index.html')
    def index(self):
        return {'msg': 'Hello from Application 1'}

@cherrypy.expose
class Api(object):

    def GET(self):
        return {'key': 'value'}

    def POST(self):
        #todo something with the incoming request
        data = cherrypy.request.json
        return data

    def PUT(self):
        #todo something with the incoming request
        data = cherrypy.request.json
        return data

    def DELETE(self):
        #todo something with the incoming request
        data = cherrypy.request.json
        return data
