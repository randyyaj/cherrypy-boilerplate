import cherrypy

__author__ = "Randy Yang (https://www.gitlab.com/randyyaj)"
__license__ = "MIT"
__version__ = "0.1.0"

class MakoTemplateTool(cherrypy.Tool):
    """
    MakoTemplateTool - Uses the MakoTemplatePlugin module to fetch templates
    Annotate an endpoint to return a rendered template using the response body 
    as the template parameter. Register this tool via: 
        
        cherrypy.tools.mako = MakoTemplateTool()

    Example:
    
        @cherrypy.tools.mako(template_name='index.html')
        index(self):
            return {'msg':'hello world!'}
    """
    def __init__(self):
        cherrypy.Tool.__init__(self, 'before_finalize',
                               self._render_template,
                               priority=10)

    def _render_template(self, template_name=None):
        if cherrypy.response.status and cherrypy.response.status > 399:
            return

        data = cherrypy.response.body or {}
        cherrypy.log(f'response body: {data}')

        if template_name and isinstance(data, dict):
            template = cherrypy.engine.publish("mako-lookup-template", template_name).pop()
            cherrypy.response.body = template.render(**data)
